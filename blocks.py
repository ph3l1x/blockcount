#!/usr/bin/python

import sys
import inspect
import argparse

DEBUG=True

def debug(msg, *args):
	if DEBUG:
		caller = inspect.stack()[1][3]
		parameter = inspect.stack()[1][4][0].strip()\
			.lstrip('debug(')\
			.rstrip(')')\
			.split(',')
		if len(args) == 0:
			print >> sys.stderr, 'DEBUG <{}>, {}: {}'\
				.format(caller, parameter[0], msg)
		else:
			print >> sys.stderr, 'DEBUG <{}>: {}'\
				.format(caller, msg)
			for i in range(0, len(args)):
				print >> sys.stderr, '    - {}: {}'\
					.format(parameter[i+1].strip() , args[i])

def gb_in_blocks(size):
	GB = 1024**3
	BLOCKSIZE = 512
	blocks = (size*GB)/BLOCKSIZE
	return blocks

def create_blocks(blocks_on_disk, outfile, chunk_size=1000, check_size=50):
	debug('args', blocks_on_disk, chunk_size, check_size)
	GB = 1024**3
	BLOCKSIZE=512
	chunks = blocks_on_disk / chunk_size
	debug(chunks)
	fh = open(outfile,'w')
	choise = str()
	for chunk in range(0, chunks):
		start = chunk*chunk_size+check_size
		end = chunk*chunk_size+chunk_size
		debug('chunk', chunk, chunks, start, end, end-start)
		if not choise == 'A':
			print 'generate this list? Yes/No/All: ', 
			choise = sys.stdin.readline().strip().upper()
		if choise == 'Y' or choise == 'A':
			#fh.write( '\n'.join(map(str, range(start, end))) )
			i = start
			while i <= end:
				fh.write(str(i)+'\n')
				i += 1
	fh.close()

def si_to_blocks(si):
	BLOCKSIZE = 512
	value = float(si.upper().rstrip('TGMKB'))
	si_praefix = si.upper().lstrip('0123456789')
	if si_praefix == 'TB':
		return int(value*1024**4/BLOCKSIZE)
	if si_praefix == 'GB':
		return int(value*1024**3/BLOCKSIZE)
	if si_praefix == 'MB':
		return int(value*1024**2/BLOCKSIZE)
	if si_praefix == 'KB':
		return int(value*1024/BLOCKSIZE)
	if si_praefix == 'B':
		return int(value)
	else:
		return sys.exit(1)

def main():
	parser = argparse.ArgumentParser(description='generates lists of blocks not to check for partial badblocks-checks.')
	parser.add_argument('file', metavar='FILE', type=str)
	parser.add_argument('-d', '--disksize')
	parser.add_argument('-s', '--slice')
	parser.add_argument('-c', '--check')
	args = parser.parse_args()
	debug('foo')
	debug(args)
	create_blocks(si_to_blocks(args.disksize), args.file, si_to_blocks(args.slice), si_to_blocks(args.check))

def test():
	debug(si_to_blocks(sys.argv[1]))

if __name__ == "__main__":
	main()
