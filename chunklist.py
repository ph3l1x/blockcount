#!/usr/bin/python

import sys
from debug import debug

class chunklist(list):
	def __init__(self):
		self.last = None
		self.lastaction = None
	def updatechunk(self, value, chunk=None):
		if chunk == None:
			chunk = len(self)-1
		self[chunk] = self[chunk][0], value
		self.last = value
		self.lastaction = 'updatechunk'
	def addnewchunk(self, value):
		self.append(tuple([value, value]))
		self.last = value
		self.lastaction = 'addnewchunk'
	def update(self, value):
		if len(self) == 0:
			self.addnewchunk(value)
		else:
			if self.last+1 == value:
				self.updatechunk(value)
			else:
				self.addnewchunk(value)
	def chunklength(self, chunk):
		return chunk[1]-chunk[0]
	def repeatlastaction(value):
		if self.lastaction == 'updatechunk':
			self.updatechunk(value)
		elif self.lastaction == 'addnewchunk':
			self.addnewchunk(value)
		else:
			debug('nothing done yet.')
			return False
	lastchunk = lambda self: self[len(self)-1]
	def numblocks(self):
		count=0
		for begin, end in self:
			count+=end-begin
		return count


def si_to_blocks(si, blocksize = 512):
	value = float(si.upper().rstrip('TGMKB'))
	si_praefix = si.upper().lstrip('0123456789')
	if si_praefix == 'TB':
		pot=4
	elif si_praefix == 'GB':
		pot=3
	elif si_praefix == 'MB':
		pot=2
	elif si_praefix == 'KB':
		pot=1
	elif si_praefix == 'B':
		pot=0
	else:
		return False
	return int(value*1024**pot/blocksize)

def blocks_to_si(blocks, si='G', blocksize=512):
	if si == 'G':
		pot=3
	elif si == 'T':
		pot=4
	elif si == 'M':
		pot=2
	elif si == 'K':
		pot=1
	else:
		return False
	return blocks*blocksize/1024.**pot




def test():
	c = chunklist()
	c.update(1)
	c.update(2)
	c.update(3)
	c.update(4)
	c.update(5)
	c.update(6)
	c.update(9)
	c.update(10)
	c.update(11)
	c.update(13)
	c.update(14)
	c.update(16)
	debug(c)
	for chunks in c:
		debug(chunks)

if __name__ == "__main__":
	test()
