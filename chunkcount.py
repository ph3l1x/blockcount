#!/usr/bin/python

import sys
import argparse
import chunklist
from debug import debug
#import debug

def getargs():
	parser = argparse.ArgumentParser(description='Parses badblocks files and scanns for (more or less) contiguous chunks')
	parser.add_argument('file', metavar='FILE', type=str, help='FILE to parse')
	parser.add_argument('-t', '--threshold', type=float, help='the amount of not broken blocks which will be skiped for getting bigger chunks. If there are 3 not broken blocks between two chunks they may breake some day also.')
	parser.add_argument('-s', '--si', type=str, default='K', choices='TGMKBtgmkb', help='select si-type for human-readability')
	return parser.parse_args()

def test():
	print 'test'

def blockline(begin, end, gap, si, i):
	return '{n:4}: {b:10} ({bg:10}) - {e:10} ({eg:10}) : {l:5} ({lg:10}) -> {last:5} ({lastg:10})'.format(b=begin,\
		bg=round(chunklist.blocks_to_si(begin, si=si), 6),\
		e=end,\
		eg=round(chunklist.blocks_to_si(end, si=si), 6),\
		l=end-begin+1,\
		lg=round(chunklist.blocks_to_si(end-begin+1, si=si), 6),\
		i=i,\
		last=gap,\
		lastg=round(chunklist.blocks_to_si(gap, si=si), 6),\
		n=i+1 )

def main():
	args = getargs()
	infile = open(args.file, 'r')
	chunks = chunklist.chunklist()
	#la = str()
	for line in infile:
		#debug.debug(line.strip())
		chunks.update(int(line))
		"""if  not chunks.lastaction == la and chunks.lastaction == 'addnewchunk':
			debug('newchunk', chunks.lastchunk() )
		la = chunks.lastaction"""
	#debug(chunks)
	i=0
	si=args.si.upper()
	print >> sys.stderr, '{} bad blocks in {} chunks in {}. {} errors per chunk.'.format(chunks.numblocks(),
			len(chunks),
			args.file,
			chunks.numblocks()/float(len(chunks)) )
	last_block=0
	for begin, end in chunks:
		#debug.debug('chunk', chunk, chunks.chunklength(chunk))
		gap=begin-last_block
		last_block=end
		print blockline(begin, end, gap, si, i)
		i+=1

if __name__ == "__main__":
	debug.DEBUG=True
	main()
