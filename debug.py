#!/usr/bin/python

import inspect
import sys
import time
import re

DEBUG=None

def debug(msg, *args):
	caller=inspect.stack()[1][3]
	parameter=inspect.stack()[1][4][0].strip()\
		.lstrip('debug(')\
		.rstrip(')')\
		.split(',')
	if len(args) == 0:
		print >> sys.stderr, 'DEBUG <{}>, {}: {}'\
			.format(caller, parameter[0], msg)
	else:
		print >> sys.stderr, 'DEBUG <{}>: {}'\
			.format(caller, msg)
		for i in range(0, len(args)):
			print >> sys.stderr, '    - {}: {}'\
				.format(parameter[i+1].strip() , args[i])
"""
class debugger(list):
	def __init__(self, debug=True):
		self.DEBUG=debug
		self.log_level=1
		self.default_log_level=1
	
	def printMsgObj(self, msg_obj):
		print >> sys.stderr, repr(msg_obj)
	
	def msg(self, msg, *args):
		if self.DEBUG:
			#print >> sys.stderr, self.getObject(msg, args)
			msg_obj = self.getObject(msg, args)
			self.printMsgObj(msg_obj)
	
	def getObject(self, msg, values):
		msg_time = '{}:{}:{}'.format(time.localtime().tm_hour, time.localtime().tm_min, time.localtime().tm_sec)
		caller = inspect.stack()[2][3]
		args = self.parseCallerLine(inspect.stack()[2][4][0])
		args.pop(0) # remove first argument (msg) which is handled seperatly
		args_values = map(lambda arg, value: {'arg':arg, 'value':value}, args, values)
		return dict(time=msg_time, msg=msg, caller=caller, args=args_values)
	
	def parseCallerLine(*callerline):
		call = callerline[1].strip()
		call = re.sub('^.*msg\(', '', call)
		call = re.sub('\)$', '', call)
		return call.split(',')

"""
def test():
	print 'debug test'
	DEBUG=True
	foo='bar'
	debug(foo)
	s=['n','a','f','u']
	debug('42', s, foo)
"""	d=debugger()
	foo='bar'
	snafu=42
	fnord=['spam', 'eggs']
	d.msg('message', foo, snafu, fnord)
	d.msg('foobar')"""


if __name__ == "__main__":
	test()
