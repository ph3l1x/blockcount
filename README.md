chunkcount.py is script that will count subsequent blocks as chunks.
This is usefull for badblocks output to see what is realy broken on the disk.

usage: chunkcount.py [-h] [-t THRESHOLD] [-s {T,G,M,K,B,t,g,m,k,b}] FILE

Parses badblocks files and scanns for (more or less) contiguous chunks

positional arguments:
  FILE

optional arguments:
  -h, --help            show this help message and exit
  -t THRESHOLD, --threshold THRESHOLD
  -s {T,G,M,K,B,t,g,m,k,b}, --si {T,G,M,K,B,t,g,m,k,b}

IMPORTANT: The THRESHOLD doenst work at the moment.
